<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EditorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Serves the editor.
     *
     * @return mixed
     */
    public function index()
    {
        return view()->file(resource_path('views/editor.blade.php'));
    }

    public function save(Request $request)
    {
        dd($request);
    }

    public function open(Request $request)
    {
        dd($request);
    }

    public function export(Request $request)
    {
        dd($request);
    }
}
